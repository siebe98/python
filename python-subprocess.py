import subprocess

hostname = "www.ole.com"
response = subprocess.run(["ping", "-n", "2", hostname], capture_output=True)

print(f"return code: {response.returncode} \n")
print(f"uitvoer: {response.stdout}\n")
print(f"errors: {response.stderr}")
