import os
def get_files_in_directory(directory):
    var = []
    for element in os.listdir(directory):
        fullpath = os.path.join(directory, element)
        var.append(fullpath)

    return var
