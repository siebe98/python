import subprocess
import sys


def IsGeldigIp():
    IsGeldig = True
    with open("host_and_ports.txt") as fh:
        Inhoud = fh.readlines()
        for element in Inhoud:
            PoortEnIp = element.split(":")
            IpAdres = PoortEnIp[0].split(".")
            for element in IpAdres:
                number = int(element)
                if not(number >= 0 and number <= 255 and len(IpAdres) == 4):
                    IsGeldig = False
                
        return IsGeldig
            
def TestConnectivity():
    #Ping kan niet op poorten pingen dus kan voor deze niet met zekerheid checken. Maar aangezien 127.0.0.1 normaal altijd
    #bereikbaar is is het in dit geval geen probleem
    
    #had het met telnet geprobeerd  maar die geeft helaas zowel bij een geslaagde test als bij een niet geslaagde test
    #dezelfde returncode dus kon de connectie ook niet op deze manier testen. heb deze code er in commentaar bij staan
    
    with open("host_and_ports.txt") as fh:
        Inhoud = fh.readlines()
        for element in Inhoud:
            Hostname = element.split(":")
            Response = subprocess.run(["ping", "-n", "2", Hostname[0]], capture_output=True)
            if(Response.returncode == 0):
                print("Ping is geslaagd")
                return True
            else:
                print("Ping is niet geslaagd")
                return False
            
            #response = subprocess.run(["telnet", hostname[0], hostname[1] ], capture_output=True)
            #print(response.returncode)
    
def ConnectAndCreateFiles():
    with open("host_and_ports.txt") as fh:
        inhoud = fh.readlines()
        for element in inhoud:
            gesplitst = element.split(":")
            with open("Commands.txt") as fh:
                completed = subprocess.run(f"ssh siebe@{gesplitst[0]} -p {gesplitst[1]} -t", capture_output=True, text=True, shell=True, stdin=fh)
                #poort ping gaan checken (soort van) checken of ssh lukt of niet.
                if(completed.returncode == 127):
                    print(f"{completed} \n")
                    print("Succesvol SSH connectie gemaakt en dit bestand is aangemaakt. \n")
                else:
                    print(f"{completed} \n")
                    print("Geen succesvolle SSH connectie gemaakt, dit bestand is niet aangemaakt \n")
                
                
        
        
IsGeldig = IsGeldigIp()
if(IsGeldig == False):
    print("Opgegeven ip adres is geen geldig ip adres")
    sys.exit(0)
else:
    print("Opgegeven ip adres is in een geldig formaat")
    PingSuccesfull = TestConnectivity()
    if(PingSuccesfull == False):
        sys.exit(0)
    else:
        ConnectAndCreateFiles()
