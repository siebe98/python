def bestaat_uit_bytes(number):
    juist = True
    gesplitst = number.split(".")
    for element in gesplitst:
        num = int(element)
        if(num < 0 or num > 254):
            juist = False
            
    return juist
