import sys

def ask_for_number_sequence_IP(Message):
    #vraag ip adres, splits deze en return hem.
    IpAddress = input(f"{Message} \n")    
    return [int(elem) for elem in IpAddress.split(".")]

def ask_for_number_sequence_NetMask(Message):
    #vraag netmask, splits deze en return hem.
    NetMask = input(f"{Message} \n")
    return [int(elem) for elem in NetMask.split(".")]    

def is_valid_ip_address(NumberList):
    GeldigIP = True

    #controleer of elk element in de NumberList geldig is. Controleer of het ip adres wel 4 karakters lang is 
    for Element in NumberList:
        if not(Element >= 0 and Element <= 255 and len(NumberList) == 4):
            GeldigIP = False

    return GeldigIP

def is_valid_netmask(NumberList):
    BinaryNetmask = ""
    Counter = 0
    #counter = 0
    GeldigMask = True
    CheckingOnes = True

    #controleer of elk element in de NumberList geldig is. Controlleert ook of het ip adres wel 4 karakters lang is 
    for Element in NumberList:
        if not(Element >= 0 and Element <= 255 and len(NumberList) == 4):
            GeldigMask = False
            break
    #Voer enkel uit wanneer er geen ongeldige karakters zijn in NumberList en deze 4 karakters lang is.
    if(GeldigMask == True):
        for Element in NumberList:
            while(Counter <4):
                #Converteer naar binair
                Binair = (f"{NumberList[Counter]:08b}")
                #32 bit resultaat van subnetmask
                BinaryNetmask += Binair
                Counter += 1
        #overloop 32 bit adres en controleer op 0 en 1
        for Karakter in BinaryNetmask:
            if(Karakter == "1" and CheckingOnes == True):
                CheckingOnes = True
                #counter += 1
                GeldigMask = True
            #stop loop zodra er een 1 komt nadat er 0 was (adres niet geldig)
            elif(Karakter == "1" and CheckingOnes == False):
                GeldigMask = False
                break
            else:
                CheckingOnes = False
                GeldigMask = True

    return GeldigMask

def one_bits_in_netmask(Netmask):
    CounterWhile = 0
    Counter = 0
    BinaryNetmask = ""
    
    #overloop list netmask, zet om naar bytes en zet deze om naar een 32bit netmask
    for Element in NetMask:
        while(CounterWhile <4):
            Binair = (f"{NetMask[CounterWhile]:08b}")
            BinaryNetmask += Binair
            CounterWhile += 1
    #overloop elk karakter in 32bit netmask. Wanneer een bit gelijk is aan 1, verhoog counter met 1.
    for Karakter in BinaryNetmask:
        if(Karakter == "1"):
            Counter += 1    

    return Counter

def apply_network_mask(IPAddress, NetMask):
    AndOperatie = []
    NetworkAddressList = []
    NetworkAddress = 0
    
    #Zet beide parameters in één for lus
    for IP, Net in zip(IPAddress, NetMask):
        #And operatie op parameters
        AndOperatie = (IP & Net)
        NetworkAddressList.append(AndOperatie)
        
    #Conversie naar output zin
    NetworkAddress = ".".join(map(str, NetworkAddressList))
    print(f"Het adres van het subnet is {NetworkAddress}")
    
    return NetworkAddressList

def netmask_to_wilcard_mask(NetMask):
    Counter = 0
    WildCard = ""
    FullWildCard = []
    WildCardZonderPunt = []
    
    #overloop elk item uit lijst 
    while(Counter < 4):
        for Bit in f"{NetMask[Counter]:08b}":
            #inverteer karakters
            if(Bit == "0"):
                WildCard += "1"
            else:
                WildCard += "0"  
        
        FullWildCard = int(WildCard, 2)
        WildCard = ""
        WildCardZonderPunt.append(FullWildCard)
        Counter += 1

    #Conversie naar output zin
    WildCardAddress = ".".join(map(str, WildCardZonderPunt))
    print(f"Het wildcardmasker is {WildCardAddress}")
    
    return WildCardZonderPunt

def get_broadcast_address(NetworkAddress, WildCard):
    OrOperatie = []
    BroadcastAddress = 0
    BroadcastAddressList = []

    #Zet beide parameters in één for lus
    for Network, Wildcard in zip(NetworkAddress, WildCard):
        #Or operatie op parameters
        OrOperatie = (Network | Wildcard)
        BroadcastAddressList.append(OrOperatie)

    #Conversie naar output zin
    BroadcastAddress = ".".join(map(str, BroadcastAddressList))
    print(f"Het broadcastadres is {BroadcastAddress}")

    return BroadcastAddressList     

def prefix_length_to_max_hosts(SubnetMask):
    Max = 32
    HostBits = Max - SubnetMask
    MaxHostAdresses = (2**HostBits)-2
    print(f"Het maximaal aantal hosts op dit subnet is {MaxHostAdresses}")

    
        
    
IP = ask_for_number_sequence_IP("Wat is het IP-adres?")
NetMask = ask_for_number_sequence_NetMask("Wat is het subnetmasker?")
GeldigIP = is_valid_ip_address(IP)
GeldigMask = is_valid_netmask(NetMask)
if(GeldigIP == False or GeldigMask == False):
    print("IP-adres en/of subnetmasker is ongeldig.")
    sys.exit(0)
else:
    print("IP-adres en subnetmasker zijn geldig.")
    SubnetMask = one_bits_in_netmask(NetMask)
    print(f"De lengte van het subnetmasker is {SubnetMask}.")
    NetworkAddress = apply_network_mask(IP, NetMask)
    WildCard = netmask_to_wilcard_mask(NetMask)
    BroadcastAddress = get_broadcast_address(NetworkAddress, WildCard)
    prefix_length_to_max_hosts(SubnetMask)
